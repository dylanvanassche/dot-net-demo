﻿<%@ Page Language="C#" MasterPageFile="~/Views/MasterPage.master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="CR" %>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="MainContentContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1>Welkom <%= Session["klantennummer"] %></h1>
        <p>Reserveer uw wagen hieronder:</p>
        <form action="Toevoegen" method="post">
            <table>
                <tr>
                    <th>Pick-up location</th>
                    <th>Vehicle type</th>
                    <th>Drop-off location</th>
                    <th>Pick-up date</th>
                    <th>Aantal dagen</th>
                    <th></th>
                </tr>
                <tr>
                    <td> 
                        <select required name="pickUpLocation">
							<%
								Data data;
								List<Locatie> locaties;
								data = (Data)ViewData["data"];
								locaties = data.Locaties;
								for(int i=0; i < locaties.Count; i++) {
									Response.Write("<option value=" + locaties[i].Nr + "/>" + locaties[i].Naam + "</option>");
								}
							%>
                        </select> 
                    </td>
                    <td> 
                        <select required name="wagen">
							<%
								data = (Data)ViewData["data"];
								List<Wagen> wagens = data.Wagens;
								for(int i=0; i < wagens.Count; i++) {
									Response.Write("<option value=" + wagens[i].Nr + "/>" + wagens[i].Naam + "</option>");
								}
							%>
                        </select> 
                    </td>
                    <td> 
                        <select required name="dropoffLocation">
							<%
								for(int i=0; i < locaties.Count; i++) {
									Response.Write("<option value=" + locaties[i].Nr + "/>" + locaties[i].Naam + "</option>");
								}
							%>
                        </select> 
                    </td>
                    <td><input required type="date" name="van"/></td>
                    <td><input required type="text" pattern="[0-9]{1,3}" name="dagen"/></td>
                    <td><input type="submit" value="Reserveer" name="submit"></td>
                </tr>
            </table>
        </form>
</asp:Content>
