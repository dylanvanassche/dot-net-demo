﻿<%@ Page Language="C#" MasterPageFile="~/Views/MasterPage.master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="MainContentContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1>Registreer</h1>
        <form method="post" action="OpslaanKlant">
            <label>Naam:</label><input type="text" name="naam"><br>
            <label>Adres:</label><input type="text" name="adres"><br>
            <label>Postcode:</label><input type="text" name="postcode" pattern="[0-9]{4}"><br>
            <label>Gemeente:</label><input type="text" name="gemeente"><br>
            <input type="submit" value="Geregistreerd" name="submit">
        </form>
</asp:Content>
