﻿using System;
namespace CR
{
	public class Wagen
	{
		private int _nr;
		private string _naam;
		private double _prijs;

		public int Nr {
			get {
				return _nr;
			}
			set
			{
				_nr = value;
			}
		}

		public string Naam
		{
			get
			{
				return _naam;
			}

			set
			{
				_naam = value;
			}
		}

		public double Prijs
		{
			get
			{
				return _prijs;
			}

			set
			{
				_prijs = value;
			}
		}

		public Wagen()
		{
		}

		public Wagen(int nr, string naam, double prijs)
		{
			_nr = nr;
			_naam = naam;
			_prijs = prijs;
		}
	}
}

