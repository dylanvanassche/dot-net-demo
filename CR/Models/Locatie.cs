﻿using System;
namespace CR
{
	public class Locatie
	{
		private int _nr;
		private string _naam;

		public int Nr
		{
			get
			{
				return _nr;
			}

			set
			{
				_nr = value;
			}
		}

		public string Naam
		{
			get
			{
				return _naam;
			}

			set
			{
				_naam = value;
			}
		}

		public Locatie()
		{
		}

		public Locatie(int nr, string naam)
		{
			_nr = nr;
			_naam = naam;
		}
	}
}

