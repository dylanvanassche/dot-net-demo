﻿using System;
using Oracle.ManagedDataAccess.Client;

namespace CR
{
	public class DbConnect
	{
		private static DbConnect _instance;

		public static DbConnect instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new DbConnect();
				}
				return _instance;
			}
		}

		public OracleConnection connect()
		{
			String oradb = "Data Source=" +
						   "(DESCRIPTION = " +
						   "(ADDRESS = (PROTOCOL = TCP)(HOST = laurel.local.thomasmore.be)(PORT = 1521))" +
						   "(CONNECT_DATA =" +
						   "(SERVER = DEDICATED)" +
						   "(SID = erp)));" + 
						   "User Id=e402;Password=e402;";

			Oracle.ManagedDataAccess.Client.OracleConnection conn = new Oracle.ManagedDataAccess.Client.OracleConnection(oradb);

			conn.Open();
			return conn;
		}
	}
}

