﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CR.Controllers
{
    public class CRController : Controller
    {
        public ActionResult Index()
        {
			Console.WriteLine("CRController Index");
            return View ();
        }

		public ActionResult Reserveer(string klantennummer)
		{
			Console.WriteLine("CRController Reserveer");
			Wagen wagen1 = new Wagen(0, "Dikke BMW", 144.454);
			Wagen wagen2 = new Wagen(1, "Klein bolleke", 11.634);
			Locatie locatie1 = new Locatie(0, "De rest is parking");
			Locatie locatie2 = new Locatie(1, "Brussel is een continent");
			Data data = new Data();
			data.AddWagen(wagen1);
			data.AddWagen(wagen2);
			data.AddLocatie(locatie1);
			data.AddLocatie(locatie2);
			ViewData["data"] = data;
			Session["klantennummer"] = klantennummer;
			return View ();
		}

		public ActionResult NieuweKlant()
		{
			Console.WriteLine("CRController nieuwe klant maken");
			return View ();
		}

		public ActionResult OpslaanKlant(string naam, string adres, string postcode, string gemeente)
		{
			Console.WriteLine("CRController opslaan nieuwe klant");
			Session["klantennummer"] = 123456789;
			return RedirectToAction("Reserveer"); // doorsturen
		}

		public ActionResult Toevoegen(DateTime van, string dagen, string pickUpLocation, string dropoffLocation, string wagen)
		{
			Console.WriteLine("CRController Toevoegen");
			ViewData["van"] = van;
			ViewData["dagen"] = dagen;
			ViewData["pickUpLocation"] = pickUpLocation;
			ViewData["dropoffLocation"] = dropoffLocation;
			ViewData["wagen"] = wagen;
			return View ();
		}
    }
}
