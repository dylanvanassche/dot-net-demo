﻿using System;
using System.Collections.Generic;

namespace CR
{
	public class Data
	{
		private List<Locatie> _locaties = new List<Locatie>();
		private List<Wagen> _wagens = new List<Wagen>();

		public List<Locatie> Locaties
		{
			get
			{
				return _locaties;
			}

			set
			{
				_locaties = value;
			}
		}

		public List<Wagen> Wagens
		{
			get
			{
				return _wagens;
			}

			set
			{
				_wagens = value;
			}
		}

		public Data()
		{
		}

		public Data(List<Locatie> locaties, List<Wagen> wagens)
		{
			_locaties = locaties;
			_wagens = wagens;
		}

		public void AddLocatie(Locatie locatie)
		{
			_locaties.Add(locatie);
		}

		public void AddWagen(Wagen wagen)
		{
			_wagens.Add(wagen);
		}
	}
}

